[TOC]

**解决方案介绍**
===============
该解决方案帮助您无服务器架构实现弹性云服务器ECS 日志的采集、分析、告警以及存档，基于云日志服务LTS 实时采集弹性云服务器的日志数据，通过函数工作流的LTS 触发器自动获取日志数据，并实现对日志中告警信息的分析，通过消息通知服务SMN 将告警信息推送给用户，并存储到对象存储OBS桶中进行存档。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/serverless-real-time-log-analysis.html](https://www.huaweicloud.com/solution/implementations/serverless-real-time-log-analysis.html)

**架构图**
---------------
![方案架构](./document/serverless-real-time-log-analysis.png)

**架构描述**
---------------
该解决方案会部署如下资源：
- 创建对象存储服务OBS，用于存储告警日志；
- 函数工作流，只需编写业务函数代码并设置运行的条件，即可以弹性、免运维、高可靠的方式运行；
- 在SMN消息通知服务创建主题，用于推送日志中的告警信息；
- 创建云日志服务LTS日志组和日志流，用于管理采集到的日志。

**组织结构**
---------------

``` lua
huaweicloud-solution-serverless-real-time-log-analysis
├──serverless-real-time-log-analysis.tf.json -- 资源编排模板
├──functiongrap 
	├──serverless-real-time-log-analysis.py -- 函数文件



```
**开始使用**
---------------

1.登录[云日志服务LTS控制台](https://console.huaweicloud.com/lts/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/cts/manager/groups)，查看创建的日志组、日志流。

图1 云日志服务LTS控制台

![云日志服务LTS控制台](./document/readme-image-001.png)

2.选择主机管理，单击“安装ICAgent”。详细步骤参考[安装ICAgent](https://support.huaweicloud.com/qs-lts/lts_0829.html)。

图2 安装ICAgent

![安装ICAgent](./document/readme-image-002.png)

3.选择日志接入，单击“云主机ECS-文本日志”。详细步骤参考[接入日志](https://support.huaweicloud.com/qs-lts/lts_08302.html)。

图3 接入日志

![接入日志](./document/readme-image-003.png)

4.选择日志管理，单击创建的日志组的名称，在日志内容即可查询采集到的日志。

图4 查看日志

![查看日志](./document/readme-image-004.png)

5.登录接收告警信息邮箱，单击订阅确认，即接收采集到的告警信息。

图5 告警信息

![告警信息](./document/readme-image-005.png)

6.登录[对象存储服务控制台](https://console.huaweicloud.com/console/?region=cn-north-4&locale=zh-cn#/obs/manager/buckets)单击创建的OBS桶名称，即可查看保存的告警日志。

图6 对象存储服务控制台

![对象存储服务控制台](./document/readme-image-006.png)

图7 查看告警日志

![查看告警日志](./document/readme-image-007.png)

